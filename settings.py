import os
import time

patterns = {
    "whatsapp_scan_me_img": "//img[@alt='Scan me!']",
    "whatsapp_search_new": "//div[@title='Новый чат']",
    "whatsapp_search_input": "//input[@title='Поиск контактов']",
    "whatsapp_check_exist": "//*[contains(text(), 'Контакты')]",
    "whatsapp_message_input": "//*[@id='main']/footer/div[1]/div[2]/div/div[2]",
    "whatsapp_message_submmit": "//*[@id='main']/footer/div[1]/div[3]/button",
}

def getLogPath(filename):
    """ Return full path by date format ({year}/{month}/{day}-{filename}.log) """
    year, month, day = time.strftime("%Y %m %d", time.localtime()).split()
    folder = "logs/{}/{}".format(year, month)
    if not os.path.isdir(folder):
        os.makedirs(folder)
    return "{}/{}-{}.log".format(folder, day, filename)

def saveLog(value, *, prefix, target):
    """ Save debug reports to 'logs' directory"""
    hour, minute, second = time.strftime("%H %M %S", time.localtime()).split()
    with open(getLogPath(target), "a") as f:
        f.write("{}:{}:{} ({})\t{}\n".format(hour, minute, second, prefix, value))

SOCKET_SERVER = ("", 9090)
SOCKET_MESSAGE_LENGTH = 256
STATE_ACTIVE = 7
STATE_PREVIOUSLY = 8
STRING_LENGTH = 32
RUSSIA_MOBILE_BEGIN = "89"
RUSSIA_MOBILE_LENGTH = 11
QUEUE_REMOVE_TIMEOUT_SEC = 60*60
QUEUE_UPDATE_TIMEOUT_SEC = 30
ORDER_MAX_TIMEOUT_MIN = 9
API_TOKEN=""
API_URL = "?.tm.taxi"
API_PORT = ""
SSL_VERIFY = False
API_CODE_SUCCESS = 0
URL_FORMAT = "https://{}:{}/{}/1.0/{}?{}"