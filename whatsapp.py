from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from settings import getLogPath, patterns as pttn

class Whatsapp():
    """Whatsapp selenium class"""

    def __init__(self, driver=webdriver.Firefox, url="https://web.whatsapp.com/"):
        self.url = url
        self.driver = driver(log_path=getLogPath("geckodriver"))

    def __enter__(self):
        print("Ready!")
        while self.windowActive():
            if self.checkLogged():
                break
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        print("Exit!")

    def sendReport(self, phone, text):
        elements = [
            ("whatsapp_search_new", "click", None),
            ("whatsapp_search_input", "send_keys", phone),
            ("whatsapp_check_exist", "click", None),
            ("whatsapp_search_input", "send_keys", Keys.ENTER),
            ("whatsapp_message_input", "send_keys", text + Keys.ENTER),
        ]
        try:
            for ptn, cmd, param in elements:
                element = getattr(WebDriverWait(self.driver, 1).until(
                    EC.presence_of_element_located((By.XPATH, pttn[ptn]))
                ), cmd)
                if param: element(param)
                else: element()
        except:
            try:
                self.body.send_keys(Keys.ESCAPE + Keys.ESCAPE)
            except:
                pass
            return False
        return True

    def checkLogged(self):
        try:
            self.body = self.driver.find_element_by_xpath("/html/body")
            self.body.find_element_by_xpath(pttn["whatsapp_search_new"])
            print("Logged!")
            return True
        except:
            return False

    def windowActive(self):
        try:
            if len(self.driver.window_handles) == 0 or self.driver.current_url != self.url:
                self.driver.get(self.url)
            return self.driver
        except:
            return False