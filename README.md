# Daemon for reporting to Whatsapp from Taxi-Master

### Setup:

```
pip install -r requirements.txt
```
Download [gecodriver](https://github.com/mozilla/geckodriver/releases).

### Run daemon:
```
python queue_io.py
```