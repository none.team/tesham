import socket
import settings
import threading
import taximaster as tm

class Passer:
    def __init__(self, order=None):
        self.order = order

    def getMessageText(self, data):
        order, phone, timeout, mark, color, code, state, time = data.values()
        return "Через {} мин. {} {} {}.".format(timeout, color, mark, code)

    def validate(self):
        validate = {
            "required": [
                "phone",
                "driver_timecount",
                "car_mark",
                "car_color",
                "gosnumber"
            ],
            "state": [
                "order_state"
            ],
            "string": [
                "car_mark",
                "car_color",
                "gosnumber"
            ],
            "timeout": [
                "driver_timecount",
            ],
            "phone": [
                "phone"
            ],
        }
        for key in validate["required"]:
            if not key in self.order.keys():
                return False
        for key in validate["state"]:
            if not int(self.order[key]) in [settings.STATE_ACTIVE, settings.STATE_PREVIOUSLY]:
                return False
        for key in validate["string"]:
            if len(self.order[key]) > settings.STRING_LENGTH:
                return False
        for key in validate["timeout"]:
            if int(self.order[key]) < 0 or int(self.order[key]) > settings.ORDER_MAX_TIMEOUT_MIN:
                return False
        for key in validate["phone"]:
            if len(self.order[key]) != settings.RUSSIA_MOBILE_LENGTH or self.order[key][:2] != settings.RUSSIA_MOBILE_BEGIN:
                return False
        return True

    def sendWhatsapp(self, phone, text):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.connect(settings.SOCKET_SERVER)
        sock.sendto("{}:{}".format(phone, text).encode("utf-8"), settings.SOCKET_SERVER)
        while not str(sock)[16:22] == "closed":
            data, addr = sock.recvfrom(settings.SOCKET_MESSAGE_LENGTH)
            status = data.decode("utf-8")
            if data:
                sock.close()
            if status == "success":
                print("WHP", self.order["id"], phone, text)
                return True
        return False

    def sendSms(self, phone, text):
        print("SMS", self.order["id"], phone, text)
        tm.post("send_sms", data={
            "message": text,
            "phone": phone
        })

    def run(self):
        text = self.getMessageText(self.order)
        if not self.sendWhatsapp(self.order["phone"], text):
            self.sendSms(self.order["phone"], text)