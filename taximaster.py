import json
import urllib3, urllib
import hashlib
import requests
import settings
import xmltodict

urllib3.disable_warnings()

def log(error):
    print(error)

def getSignature(value):
    return hashlib.md5("{}{}".format(value, settings.API_TOKEN).encode("utf-8")).hexdigest()

def getHeaders(params=None):
    headers = {}
    headers["Signature"] = getSignature(params)
    return headers

def prepareUrl(method):
    def preparer(*args, **kwargs):
        params, data = ("&".join(["{}={}".format(k, v) for k, v in kwargs.get("params", {}).items()]),
                        urllib.parse.urlencode(kwargs.get("data", {})))
        url = settings.URL_FORMAT.format(settings.API_URL, settings.API_PORT, kwargs.get("api", "common_api"), args[0], params)
        signature = "&signature={}".format(getSignature(params))
        return method(url+signature if kwargs.get("api") else url, params=params, data=data, key=kwargs.get("key"))
    return preparer

def parseContent(value, key=None, isPost=False):
    try:
        value = json.loads(value)
    except ValueError as e:
        value = xmltodict.parse(value)
    value = value["response"] if "response" in value else value
    success = int(value["code"]) == settings.API_CODE_SUCCESS
    return [] if not success else value["data"][key] if key in value["data"] else value["data"]

@prepareUrl
def get(url, *, params={}, data={}, key=None, api=None):
    try:
        request = requests.get(url=url, verify=settings.SSL_VERIFY, headers=getHeaders(params))
        return parseContent(request.content, key)
    except Exception as e:
        log(e)
        return []

@prepareUrl
def post(url, *, params={}, data={}, key=None, api=None):
    try:
        request = requests.post(url=url, verify=settings.SSL_VERIFY, headers=getHeaders(data), data=data.encode("utf-8"))
        return parseContent(request.content, isPost=True)
    except Exception as e:
        log(e)