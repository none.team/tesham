import time
import socket
import settings
import threading
import taximaster as tm
import passer
import whatsapp
from datetime import datetime as dt

def circle(func):
    """ Call function recursivly with timeout """
    def waiter(*args, **kwargs):
        while True:
            func()
            time.sleep(kwargs.get("sleep", .5))
    return waiter

@circle
def proccessQueue():
    """ Update queue (get new orders ('id' and 'phone')) from API """
    orders = tm.get("get_current_orders", key="orders")
    orders = filter(lambda order: order["state_id"] in [settings.STATE_ACTIVE, settings.STATE_PREVIOUSLY], orders)
    orders = filter(lambda order: not "{}:{}".format(order["id"], order["phone"]) in queue["proccess"] + list(queue["done"].keys()), orders)
    for order in orders:
        queue["proccess"].append("{}:{}".format(order["id"], order["phone"]))

@circle
def doneQueue():
    """ Get more information about order and send it to passer """
    while len(queue["proccess"]):
        order = dict(zip(['id', 'phone'], queue["proccess"][0].split(":")))
        response = tm.get("get_info_by_order_id", params={
            "order_id": order["id"],
            "fields": "CAR_MARK-CAR_COLOR-GOSNUMBER-DRIVER_TIMECOUNT-SOURCE_TIME-ORDER_STATE"
        }, api="tm_tapi")
        if response != []:
            for key in response:
                order[key.lower()] = response[key]
            if order["driver_timecount"] == None:
                order["driver_timecount"] = str(int((dt.strptime(response["SOURCE_TIME"], "%Y%m%d%H%M%S")-dt.now()).total_seconds()//60))
            sender = passer.Passer(order)
            if sender.validate():
                sender.run()
                queue["done"][queue["proccess"][0]] = time.time() + settings.QUEUE_REMOVE_TIMEOUT_SEC
            else:
                if int(order["driver_timecount"]) >= 0:
                    queue["proccess"].append(queue["proccess"][0])
            queue["proccess"].pop(0)

@circle
def clearQueue():
    """ Clear items where passed timeout """
    for key in list(filter(lambda k: queue["done"][k] < time.time(), queue["done"].keys())):
        del queue["done"][key]

@circle
def whatsappQueue():
    """ Manage 'whatsapp' queue from incoming socket connections. """
    message, (ip, addr) = sock.recvfrom(settings.SOCKET_MESSAGE_LENGTH)
    if message:
        phone, text = message.decode("utf-8").split(":")
        if whatsappActive:
            queue["whatsapp"].append(((ip, addr), phone, text))
        else:
            sock.sendto(b"fail", (ip, addr))

@circle
def runWhatsapp():
    """ Run selenium browser and try to send messages from 'whatsapp' queue """
    global whatsappActive, queue
    with whatsapp.Whatsapp() as browser:
        while browser.windowActive():
            whatsappActive = True
            if len(queue["whatsapp"]):
                (ip, addr), phone, text = queue["whatsapp"][0]
                sock.sendto(b"success" if browser.sendReport(phone, text) else b"fail", (ip, addr))
                queue["whatsapp"].remove(queue["whatsapp"][0])
    whatsappActive = False

@circle
def saveQueue():
    with open("queue.csv", "w") as file:
        rows = ""
        for key, value in queue["done"].items():
            rows += "{};{}\n".format(key,value)
        file.write(rows)

def loadQueue():
    done = {}
    with open("queue.csv", "r") as file:
        for line in filter(lambda line: line != "\n", file.readlines()):
            key, value = line.split(";")
            done[key] = float(value)
    return done


whatsappActive = False
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(settings.SOCKET_SERVER)

queue = {
    "proccess": [],
    "whatsapp": [],
    "done": loadQueue()
}

threads = [
    threading.Thread(target=proccessQueue, kwargs={"sleep": settings.QUEUE_UPDATE_TIMEOUT_SEC}),
    threading.Thread(target=doneQueue),
    threading.Thread(target=clearQueue, kwargs={"sleep": settings.QUEUE_UPDATE_TIMEOUT_SEC}),
    threading.Thread(target=runWhatsapp),
    threading.Thread(target=whatsappQueue, kwargs={"sleep": 0}),
    threading.Thread(target=saveQueue, kwargs={"sleep": settings.QUEUE_UPDATE_TIMEOUT_SEC}),
]
for thread in threads:
    thread.start()
